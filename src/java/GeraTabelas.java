
import br.com.easypoint.dao.*;
import br.com.easypoint.model.*;
import br.com.easypoint.util.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author msouza
 */
public class GeraTabelas {

    public static void main(String[] args) {

        /* TESTE BUSCA POR ID
        FuncionarioDAO f = new FuncionarioDAO();
        Funcionario fu = f.buscarPorId(new Long("1"));
        
        System.out.println("ID: " + fu.getId() + "\nNome: " + fu.getNome());
         */
 /*
        GERAR TABELAS
         */
        int gerarTabelas = JOptionPane.showConfirmDialog(null, "Gerar tabelas?", "Confirmação", JOptionPane.YES_NO_OPTION);
        if (gerarTabelas == JOptionPane.YES_OPTION) {
            AnnotationConfiguration config = new AnnotationConfiguration();
            config.configure();
            new SchemaExport(config).create(true, true);

//            int inserirDadosExemplo = JOptionPane.showConfirmDialog(null, "Inserir dados de exemplo?", "Confirmação", JOptionPane.YES_NO_OPTION);
//            if (inserirDadosExemplo == JOptionPane.YES_OPTION) {
//                SessionFactory sessionFactory = config.buildSessionFactory();
//                Session session = sessionFactory.getCurrentSession();
//
//                session.beginTransaction();
//
//                Empresa emp = new Empresa();
//                emp.setId(Long.parseLong("1"));
//
//                Administrador adm = new Administrador();
//                adm.setNome("Matheus Souza");
//                adm.setEmail("matheusviegasdesouza@gmail.com");
//                adm.setSenha("senhateste");
//                adm.setUsuario("matheus");
//                adm.setEmpresa(emp);
//                session.save(adm);
//                session.getTransaction().commit();
//
//            }
        }

        /*AnnotationConfiguration config = new AnnotationConfiguration();
        config.configure();
        SessionFactory sessionFactory = config.buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();

        session.beginTransaction();

        Empresa emp = new Empresa();
        emp.setId(Long.parseLong("1"));

        Administrador adm = new Administrador();
        adm.setNome("Matheus Souza");
        adm.setEmail("matheusviegasdesouza@gmail.com");
        adm.setSenha("senhateste");
        adm.setUsuario("matheus");
        adm.setEmpresa(emp);
        session.save(adm);
        session.getTransaction().commit();*/
 /*
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Administrador> adm = session.createCriteria(Administrador.class).list();
        session.close();
        
        System.out.println("ADMINISTRADORES");
        for(Administrador a : adm){
            System.out.println("Nome: " + a.getNome());
            System.out.println("Usuario: " + a.getUsuario());
            System.out.println("Email: " + a.getEmail());
            System.out.println("Cidade: " + a.getEmpresa().getEndereco().getCidade().getNome());
        }*/
 /*
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<EscalaHorario> escalaHorario = session.createCriteria(EscalaHorario.class).list();
        session.close();
        
        System.out.println("ADMINISTRADORES");
        String horario = "";
        for(EscalaHorario esc : escalaHorario){
            System.out.println("Escala: " + esc.getEscala().getNome());
            if(esc.getHorario().getDia() == "Segunda"){
                horario += esc.getHorario().getHoraentrada() + " - " + esc.getHorario().getHorasaida() + " ";
            }else{
                horario = esc.getHorario().getHoraentrada() + " - " + esc.getHorario().getHorasaida() + " ";
            }
        
            System.out.println("Expediente: " + horario);

        }
         */
 /*
        // TESTE BUSCA POR ID
        PaisDAO pdao = new PaisDAO();
        Pais p = pdao.buscarPorId(new Long("3"));

        System.out.println("ID: " + p.getId() + "\nNome: " + p.getNome());
         */
 /*
        EstadoDAO estDAO = new EstadoDAO();
        PaisDAO pDAO = new PaisDAO();
        Estado estado = new Estado();
        estado.setNome("Rio de Janeiro");
        estado.setSigla("RJ");
        Pais p = new Pais();
        p.setNome("Brasil");
        p = pDAO.salvar(p);
        estado.setPais(p);
        estDAO.salvar(estado);
        
         */
 /*
        EscalaDAO escDAO = new EscalaDAO();
        List<Escala> escalas = escDAO.listar();
        
        for(Escala e : escalas){
            System.out.println("ID: " + e.getId() + " Nome: " + e.getNome());
        }
         */
//         try {
//            Session session = HibernateUtil.getSessionFactory().openSession();
//            String jpql = "FROM br.com.easypoint.model.Estado WHERE pais_id = :idPais";
//            Query query = session.createQuery(jpql);
//            query.setParameter("idPais", Long.parseLong("1"));
//            
//            List<Estado> estados = (List<Estado>) query.list();
//            for(Estado e : estados){
//                System.out.println("ID: " + e.getId() + " Nome: " + e.getNome() + " Sigla: " + e.getSigla());
//            }
//            
//            session.close();
//
//        } catch (Exception e) {
//            throw e;
//        }
//        
        //INSERE ESCALA EXEMPLO
//int opcao = JOptionPane.showConfirmDialog(null, "Inserir escala exemplo?", "Confirmação", JOptionPane.YES_NO_OPTION);
//        if (opcao == JOptionPane.YES_OPTION) {
//            HorarioDAO horarioDAO = new HorarioDAO();
//            List<Horario> horarios = horarioDAO.listar();
//            for (Horario h : horarios) {
//                System.out.println("ID: " + h.getId() + " Dia: " + h.getDia() + " E: " + h.getHoraentrada() + " S: " + h.getHorasaida());
//            }
//
//            EscalaDAO eDAO = new EscalaDAO();
//            Escala e = new Escala();
//
//            e.setNome("Assador");
//            e.setHorarios(horarios);
//
//            eDAO.salvar(e);
//
//        }

        /* SELECT PARA VERIFICAR SE A ESCALA FOI INSERIDA
    select escala.nome, horario.dia, horario.horaentrada, horario.horasaida from escala inner join escala_horario on escala.id = escala_horario.escala_id 
inner join horario on horario.id = escala_horario.horarios_id;
         */
    }

}
