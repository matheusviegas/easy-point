/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author msouza
 */
public abstract class Util {

    public Date formataDataDMH(String data) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date dataFormatada = null;
        try {
            dataFormatada = sdf.parse(data);
        } catch (ParseException e) {
        }
        return dataFormatada;
    }

    public Date formataHoraHM(String hora) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date horaFormatada = null;
        try {
            horaFormatada = sdf.parse(hora);
        } catch (ParseException e) {
        }
        return horaFormatada;
    }
}
