/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.Horario;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class HorarioDAO {
    
    Session session;

    public HorarioDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void salvar(Horario horario){
        session.beginTransaction();
        session.saveOrUpdate(horario);
        session.getTransaction().commit();
        session.close();
        
    }
    public List<Horario> listar(){
        List<Horario> horarios = session.createCriteria(Horario.class).list();
        session.close();
        return horarios;
    }
    public void remover(Horario horario){
        session.beginTransaction();
        session.delete(horario);
        session.getTransaction().commit();
        session.close();
    }
    
    public Horario buscarPorId(Long id){
        session.beginTransaction();
        Horario hor = (Horario) session.get(Horario.class, id);
        session.close();
        return hor;
    }
    
    
    
}
