/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.Cidade;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class CidadeDAO {
    
    Session session;

    public CidadeDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void salvar(Cidade cidade){
        session.beginTransaction();
        session.saveOrUpdate(cidade);
        session.getTransaction().commit();
        session.close();
        
    }
    public List<Cidade> listar(){
        List<Cidade> client = session.createCriteria(Cidade.class).list();
        session.close();
        return client;
    }
    public void remover(Cidade cidade){
        session.beginTransaction();
        session.delete(cidade);
        session.getTransaction().commit();
        session.close();
    }
    
    public Cidade buscarPorId(Long id){
        session.beginTransaction();
        Cidade cid = (Cidade) session.get(Cidade.class, id);
        session.close();
        return cid;
    }
    
    public List<Cidade> preencheCidade(Long idEstado) throws Exception {

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String jpql = "FROM br.com.easypoint.model.Cidade WHERE estado_id = :idEstado";
            Query query = session.createQuery(jpql);
            query.setParameter("idEstado", idEstado);

            if (!query.list().isEmpty()) {
                return (List<Cidade>) query.list();
            }

        } catch (Exception e) {
            throw e;
        }

        return null;
    }
    
}
