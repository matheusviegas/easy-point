/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.Administrador;
import br.com.easypoint.model.Estado;
import br.com.easypoint.model.Funcionario;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class FuncionarioDAO {
    
    Session session;

    public FuncionarioDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void salvar(Funcionario funcionario){
        session.beginTransaction();
        session.saveOrUpdate(funcionario);
        session.getTransaction().commit();
        session.close();
        
    }
    public List<Funcionario> listar(){
        List<Funcionario> client = session.createCriteria(Funcionario.class).list();
        session.close();
        return client;
    }
    public void remover(Funcionario funcionario){
        session.beginTransaction();
        session.delete(funcionario);
        session.getTransaction().commit();
        session.close();
    }
    
    public Funcionario buscarPorId(Long id){
        session.beginTransaction();
        Funcionario func = (Funcionario) session.get(Funcionario.class, id);
        session.close();
        return func;
    }
  
    
    
}
