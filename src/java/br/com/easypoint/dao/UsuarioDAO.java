package br.com.easypoint.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import br.com.easypoint.model.UsuarioTeste;
import br.com.easypoint.util.HibernateUtil;

public class UsuarioDAO {

    private Session session;

    public UsuarioTeste verificaDados(UsuarioTeste usuario) throws Exception {
        UsuarioTeste u = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String jpql = "FROM br.com.easypoint.model.Usuario WHERE usuario = :nomeDeUsuario and senha = :senha";
            Query query = session.createQuery(jpql);
            query.setParameter("nomeDeUsuario", usuario.getUsuario());
            query.setParameter("senha", usuario.getSenha());

            if (!query.list().isEmpty()) {
                u = (UsuarioTeste) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        }

        return u;
    }
}
