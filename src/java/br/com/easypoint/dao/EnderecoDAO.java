/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.Endereco;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class EnderecoDAO {
    
    Session session;

    public EnderecoDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public Endereco salvar(Endereco endereco){
        session.beginTransaction();
        session.saveOrUpdate(endereco);
        session.getTransaction().commit();
        session.flush();
        session.close();
        return endereco;
        
    }
    public List<Endereco> listar(){
        List<Endereco> client = session.createCriteria(Endereco.class).list();
        session.close();
        return client;
    }
    public void remover(Endereco endereco){
        session.beginTransaction();
        session.delete(endereco);
        session.getTransaction().commit();
        session.close();
    }
    
    public Endereco buscarPorId(Long id){
        session.beginTransaction();
        Endereco end = (Endereco) session.get(Endereco.class, id);
        session.close();
        return end;
    }
    
    
    
}
