/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.Cargo;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class CargoDAO {
    
    Session session;

    public CargoDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void salvar(Cargo cargo){
        session.beginTransaction();
        session.saveOrUpdate(cargo);
        session.getTransaction().commit();
        session.close();
        
    }
    public List<Cargo> listar(){
        List<Cargo> client = session.createCriteria(Cargo.class).list();
        session.close();
        return client;
    }
    public void remover(Cargo cargo){
        session.beginTransaction();
        session.delete(cargo);
        session.getTransaction().commit();
        session.close();
    }
    
    public Cargo buscarPorId(Long id){
        session.beginTransaction();
        Cargo car = (Cargo) session.get(Cargo.class, id);
        session.close();
        return car;
    }
    
    
    
}
