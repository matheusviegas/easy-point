/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.Empresa;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class EmpresaDAO {
    
    Session session;

    public EmpresaDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void salvar(Empresa empresa){
        session.beginTransaction();
        session.saveOrUpdate(empresa);
        session.getTransaction().commit();
        session.close();
        
    }
    public List<Empresa> listar(){
        List<Empresa> client = session.createCriteria(Empresa.class).list();
        session.close();
        return client;
    }
    public void remover(Empresa empresa){
        session.beginTransaction();
        session.delete(empresa);
        session.getTransaction().commit();
        session.close();
    }
    
    public Empresa buscarPorId(Long id){
        session.beginTransaction();
        Empresa emp = (Empresa) session.get(Empresa.class, id);
        session.close();
        return emp;
    }
    
}
