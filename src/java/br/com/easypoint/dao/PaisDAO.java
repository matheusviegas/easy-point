/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.Pais;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class PaisDAO {
    
    Session session;

    public PaisDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public Pais salvar(Pais pais){
        session.beginTransaction();
        session.saveOrUpdate(pais);
        session.getTransaction().commit();
        session.flush();
        session.close();
        return pais;
    }
    public List<Pais> listar(){
        List<Pais> client = session.createCriteria(Pais.class).list();
        session.close();
        return client;
    }
    public void remover(Pais pais){
        session.beginTransaction();
        session.delete(pais);
        session.getTransaction().commit();
        session.close();
    }
    
    public Pais buscarPorId(Long id){
        session.beginTransaction();
        Pais p = (Pais) session.get(Pais.class, id);
        session.close();
        return p;
    }
    
    
    
}
