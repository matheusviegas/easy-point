/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.FuncionarioAntigo;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class FuncionarioDAOAntigo {
    
    Session session;

    public FuncionarioDAOAntigo() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void salvar(FuncionarioAntigo funcionario){
        session.beginTransaction();
        session.saveOrUpdate(funcionario);
        session.getTransaction().commit();
        session.close();
        
    }
    public List<FuncionarioAntigo> listar(){
        List<FuncionarioAntigo> client = session.createCriteria(FuncionarioAntigo.class).list();
        session.close();
        return client;
    }
    public void remover(FuncionarioAntigo funcionario){
        session.beginTransaction();
        session.delete(funcionario);
        session.getTransaction().commit();
        session.close();
    }
    
    public FuncionarioAntigo buscarPorId(Long id){
        session.beginTransaction();
        FuncionarioAntigo func = (FuncionarioAntigo) session.get(FuncionarioAntigo.class, id);
        session.close();
        return func;
    }
    
    
    
}
