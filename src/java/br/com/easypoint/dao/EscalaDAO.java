/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.Escala;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class EscalaDAO {
    
    Session session;

    public EscalaDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void salvar(Escala escala){
        session.beginTransaction();
        session.saveOrUpdate(escala);
        session.getTransaction().commit();
        session.close();
        
    }
    public List<Escala> listar(){
        List<Escala> client = session.createCriteria(Escala.class).list();
        session.close();
        return client;
    }
    public void remover(Escala escala){
        session.beginTransaction();
        session.delete(escala);
        session.getTransaction().commit();
        session.close();
    }
    
    public Escala buscarPorId(Long id){
        session.beginTransaction();
        Escala esc = (Escala) session.get(Escala.class, id);
        session.close();
        return esc;
    }
    
    
    
}
