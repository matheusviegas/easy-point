package br.com.easypoint.dao;

import br.com.easypoint.model.Administrador;
import org.hibernate.Query;
import org.hibernate.Session;
import br.com.easypoint.util.HibernateUtil;

public class AdministradorDAO {

    private Session session;

    public Administrador verificaDados(Administrador administrador) throws Exception {
        Administrador adm = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            String jpql = "FROM br.com.easypoint.model.Administrador WHERE usuario = :nomeDeUsuario and senha = :senha";
            Query query = session.createQuery(jpql);
            query.setParameter("nomeDeUsuario", administrador.getUsuario());
            query.setParameter("senha", administrador.getSenha());

            if (!query.list().isEmpty()) {
                adm = (Administrador) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        }

        return adm;
    }
}
