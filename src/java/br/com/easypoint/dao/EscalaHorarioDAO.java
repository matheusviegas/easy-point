/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.EscalaHorario;
import br.com.easypoint.model.Horario;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;

/**
 *
 * @author Matheus Souza
 */
public class EscalaHorarioDAO {

    Session session;

    public EscalaHorarioDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }

    public void salvar(List<EscalaHorario> escalahorario) {
        session.beginTransaction();
        for (EscalaHorario eh : escalahorario) {
            session.saveOrUpdate(escalahorario);
        }
        session.getTransaction().commit();
        session.close();

    }

    public List<EscalaHorario> listar() {
        List<EscalaHorario> client = session.createCriteria(EscalaHorario.class).list();
        session.close();
        return client;
    }

    public void remover(EscalaHorario escalahorario) {
        session.beginTransaction();
        session.delete(escalahorario);
        session.getTransaction().commit();
        session.close();
    }

    public EscalaHorario buscarPorId(Long id) {
        session.beginTransaction();
        EscalaHorario esc = (EscalaHorario) session.get(EscalaHorario.class, id);
        session.close();
        return esc;
    }

}
