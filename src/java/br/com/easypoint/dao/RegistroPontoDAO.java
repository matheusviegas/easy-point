/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.dao;

import br.com.easypoint.model.RegistroPonto;
import br.com.easypoint.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author Matheus Souza
 */
public class RegistroPontoDAO {
    
    Session session;

    public RegistroPontoDAO() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void salvar(RegistroPonto registroponto){
        session.beginTransaction();
        session.saveOrUpdate(registroponto);
        session.getTransaction().commit();
        session.close();
        
    }
    public List<RegistroPonto> listar(){
        List<RegistroPonto> client = session.createCriteria(RegistroPonto.class).list();
        session.close();
        return client;
    }
    public void remover(RegistroPonto registroponto){
        session.beginTransaction();
        session.delete(registroponto);
        session.getTransaction().commit();
        session.close();
    }
    
    public RegistroPonto buscarPorId(Long id){
        session.beginTransaction();
        RegistroPonto rp = (RegistroPonto) session.get(RegistroPonto.class, id);
        session.close();
        return rp;
    }
    
    
    
}
