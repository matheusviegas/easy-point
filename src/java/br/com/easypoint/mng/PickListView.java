package br.com.easypoint.mng;

import br.com.easypoint.dao.HorarioDAO;
import br.com.easypoint.model.Horario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.DualListModel;

@ManagedBean
@ViewScoped
public class PickListView implements Serializable {

    private static final long serialVersionUID = 1L;
    private HorarioDAO horarioDAO = new HorarioDAO();
    private DualListModel<Horario> horarioDualList;
    private List<Horario> horariosSelecionados = new ArrayList<Horario>();
    private List<Horario> horariosRestantes = new ArrayList<Horario>();
    private List<Horario> listaDeHorarios = horarioDAO.listar();

    public HorarioDAO getHorarioDAO() {
        return horarioDAO;
    }

    public void setHorarioDAO(HorarioDAO horarioDAO) {
        this.horarioDAO = horarioDAO;
    }


    public List<Horario> getHorariosSelecionados() {
        return horariosSelecionados;
    }

    public void setHorariosSelecionados(List<Horario> horariosSelecionados) {
        this.horariosSelecionados = horariosSelecionados;
    }

    public List<Horario> getHorariosRestantes() {
        return horariosRestantes;
    }

    public void setHorariosRestantes(List<Horario> horariosRestantes) {
        this.horariosRestantes = horariosRestantes;
    }

    public List<Horario> getListaDeHorarios() {
        return listaDeHorarios;
    }

    public void setListaDeHorarios(List<Horario> listaDeHorarios) {
        this.listaDeHorarios = listaDeHorarios;
    }

    public void gravar() {
        horariosRestantes = horarioDualList.getSource();
        horariosSelecionados = horarioDualList.getTarget();

        System.out.println("Lista");
        for (Horario h : listaDeHorarios) {
            System.out.println("Dia: " + h.getDia() + " E: " + h.getHoraentrada() + " S: " + h.getHorasaida());
        }

        System.out.println("Selecionados");
        for (Horario h2 : horariosSelecionados) {
            System.out.println("Dia: " + h2.getDia() + " E: " + h2.getHoraentrada() + " S: " + h2.getHorasaida());
        }

    }

    public DualListModel<Horario> getHorarioDualList() {
        if (this.horarioDualList == null) {
            this.horarioDualList = new DualListModel<Horario>(this.listaDeHorarios, new ArrayList<Horario>());
        }
        return this.horarioDualList;
    }

    public void setHorarioDualList(DualListModel<Horario> horarioDualList) {
        this.horarioDualList = horarioDualList;
    }

}
