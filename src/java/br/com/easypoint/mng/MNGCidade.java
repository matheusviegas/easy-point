/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.CidadeDAO;
import br.com.easypoint.model.Cidade;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Matheus Souza
 */

@ManagedBean(name = "MNGCidade")
@SessionScoped
public class MNGCidade implements Serializable{
    
    private Cidade cidade = new Cidade();

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }
    
    public void salvar(){
        CidadeDAO cidadeDAO = new CidadeDAO();
        cidadeDAO.salvar(this.cidade);
        this.cidade = new Cidade();
    }
    
    public List<Cidade> listar(){
        CidadeDAO cidadeDAO = new CidadeDAO();
        return cidadeDAO.listar();
    }
    
    public void remover(Cidade cidade){
        CidadeDAO cidadeDAO = new CidadeDAO();
        cidadeDAO.remover(cidade);
    }
    
    public List<Cidade> preencheComboCidades(Long id) throws Exception {
        CidadeDAO cidadeDAO = new CidadeDAO();
        return cidadeDAO.preencheCidade(id);
    }
}
