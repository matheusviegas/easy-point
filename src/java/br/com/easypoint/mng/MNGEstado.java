package br.com.easypoint.mng;

import br.com.easypoint.dao.EstadoDAO;
import br.com.easypoint.model.Estado;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author Matheus Souza
 */
@ManagedBean(name = "MNGEstado")
@SessionScoped
public class MNGEstado implements Serializable {

    private Estado estado = new Estado();

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public void salvar() {
        EstadoDAO estadoDAO = new EstadoDAO();
        estadoDAO.salvar(this.estado);
        this.estado = new Estado();
    }

    public List<Estado> listar() {
        EstadoDAO estadoDAO = new EstadoDAO();
        return estadoDAO.listar();
    }

    public List<Estado> preencheComboEstados(Long id) throws Exception {
        System.out.println("CHEGOU");
        EstadoDAO estadoDAO = new EstadoDAO();
        return estadoDAO.preencheEstado(id);
    }

    public void remover(Estado estado) {
        EstadoDAO estadoDAO = new EstadoDAO();
        estadoDAO.remover(estado);
    }
}
