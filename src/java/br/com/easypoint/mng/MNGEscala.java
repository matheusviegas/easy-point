/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.EscalaDAO;
import br.com.easypoint.dao.HorarioDAO;
import br.com.easypoint.model.Escala;
import br.com.easypoint.model.Horario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Matheus Souza
 */
@ManagedBean(name = "MNGEscala")
@SessionScoped
public class MNGEscala implements Serializable {

    //ATRIBUTOS PARA O PICKLIST
    private HorarioDAO horarioDAO = new HorarioDAO();
    private DualListModel<Horario> horarioDualList;
    private List<Horario> horariosSelecionados = new ArrayList<Horario>();
    private List<Horario> horariosRestantes = new ArrayList<Horario>();
    private List<Horario> listaDeHorarios = horarioDAO.listar();

    private Escala escala = new Escala();

    public Escala getEscala() {
        return escala;
    }

    public void setEscala(Escala escala) {
        this.escala = escala;
    }

    public List<Horario> getHorariosSelecionados() {
        return horariosSelecionados;
    }

    public void setHorariosSelecionados(List<Horario> horariosSelecionados) {
        this.horariosSelecionados = horariosSelecionados;
    }

    public List<Horario> getHorariosRestantes() {
        return horariosRestantes;
    }

    public void setHorariosRestantes(List<Horario> horariosRestantes) {
        this.horariosRestantes = horariosRestantes;
    }

    public List<Horario> getListaDeHorarios() {
        return listaDeHorarios;
    }

    public void setListaDeHorarios(List<Horario> listaDeHorarios) {
        this.listaDeHorarios = listaDeHorarios;
    }

    public DualListModel<Horario> getHorarioDualList() {
        if (this.horarioDualList == null) {
            this.horarioDualList = new DualListModel<Horario>(this.listaDeHorarios, new ArrayList<Horario>());
        }
        return this.horarioDualList;
    }

    public void setHorarioDualList(DualListModel<Horario> horarioDualList) {
        this.horarioDualList = horarioDualList;
    }

    public void salvar() {
        this.horariosRestantes = horarioDualList.getSource();
        this.horariosSelecionados = horarioDualList.getTarget();

        EscalaDAO escalaDAO = new EscalaDAO();
        this.escala.setHorarios(horariosSelecionados);
        escalaDAO.salvar(this.escala);
        this.escala = new Escala();
    }

    public List<Escala> listar() {
        EscalaDAO escalaDAO = new EscalaDAO();
        return escalaDAO.listar();
    }

    public void remover(Escala escala) {
        EscalaDAO escalaDAO = new EscalaDAO();
        escalaDAO.remover(escala);
    }
}
