/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.EmpresaDAO;
import br.com.easypoint.model.Empresa;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Matheus Souza
 */
@ManagedBean(name = "MNGEmpresa")
@SessionScoped
public class MNGEmpresa implements Serializable {

    private Empresa empresa = new Empresa();

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void salvar() {
        EmpresaDAO empresaDAO = new EmpresaDAO();
        empresaDAO.salvar(this.empresa);
        this.empresa = new Empresa();
    }

    public List<Empresa> listar() {
        EmpresaDAO empresaDAO = new EmpresaDAO();
        return empresaDAO.listar();
    }

    public void remover(Empresa empresa) {
        EmpresaDAO empresaDAO = new EmpresaDAO();
        empresaDAO.remover(empresa);
    }
}
