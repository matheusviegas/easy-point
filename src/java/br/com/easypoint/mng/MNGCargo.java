/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.CargoDAO;
import br.com.easypoint.model.Cargo;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Matheus Souza
 */

@ManagedBean(name = "MNGCargo")
@SessionScoped
public class MNGCargo implements Serializable{
    
    private Cargo cargo = new Cargo();

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

   
    
    public void salvar(){
        CargoDAO cargoDAO = new CargoDAO();
        cargoDAO.salvar(this.cargo);
        this.cargo = new Cargo();
    }
    
    public List<Cargo> listar(){
        CargoDAO cargoDAO = new CargoDAO();
        return cargoDAO.listar();
    }
    
    public void remover(Cargo cargo){
        CargoDAO cargoDAO = new CargoDAO();
        cargoDAO.remover(cargo);
        this.cargo = new Cargo();
    }
}
