/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.EnderecoDAO;
import br.com.easypoint.model.Endereco;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Matheus Souza
 */
@ManagedBean(name = "MNGEndereco")
@SessionScoped
public class MNGEndereco implements Serializable {

    private Endereco endereco = new Endereco();

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public void salvar() {
        EnderecoDAO enderecoDAO = new EnderecoDAO();
        enderecoDAO.salvar(this.endereco);
        this.endereco = new Endereco();
    }

    public List<Endereco> listar() {
        EnderecoDAO enderecoDAO = new EnderecoDAO();
        return enderecoDAO.listar();
    }

    public void remover(Endereco endereco) {
        EnderecoDAO enderecoDAO = new EnderecoDAO();
        enderecoDAO.remover(endereco);
    }
}
