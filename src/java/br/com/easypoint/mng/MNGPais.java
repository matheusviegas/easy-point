/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.PaisDAO;
import br.com.easypoint.model.Pais;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Matheus Souza
 */
@ManagedBean(name = "MNGPais")
@SessionScoped
public class MNGPais implements Serializable {

    private Pais pais = new Pais();

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public void salvar() {
        PaisDAO paisDAO = new PaisDAO();
        paisDAO.salvar(this.pais);
        this.pais = new Pais();
    }

    public List<Pais> listar() {
        PaisDAO paisDAO = new PaisDAO();
        return paisDAO.listar();
    }

    public void remover(Pais pais) {
        PaisDAO paisDAO = new PaisDAO();
        paisDAO.remover(pais);
    }
}
