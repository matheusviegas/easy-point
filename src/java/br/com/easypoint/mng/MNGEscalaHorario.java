/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.EscalaHorarioDAO;
import br.com.easypoint.model.EscalaHorario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Matheus Souza
 */
@ManagedBean(name = "MNGEscalaHorario")
@SessionScoped
public class MNGEscalaHorario implements Serializable {

    private List<EscalaHorario> escalahorario = new ArrayList<EscalaHorario>();

    public List<EscalaHorario> getEscalahorario() {
        return escalahorario;
    }

    public void setEscalahorario(List<EscalaHorario> escalahorario) {
        this.escalahorario = escalahorario;
    }

    public void salvar() {
        EscalaHorarioDAO escalahorarioDAO = new EscalaHorarioDAO();
        escalahorarioDAO.salvar(this.escalahorario);
        this.escalahorario = new ArrayList<EscalaHorario>();
    }

    public List<EscalaHorario> listar() {
        EscalaHorarioDAO escalahorarioDAO = new EscalaHorarioDAO();
        return escalahorarioDAO.listar();
    }

    public void remover(EscalaHorario escalahorario) {
        EscalaHorarioDAO escalahorarioDAO = new EscalaHorarioDAO();
        escalahorarioDAO.remover(escalahorario);
    }
}
