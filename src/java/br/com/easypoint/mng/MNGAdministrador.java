package br.com.easypoint.mng;

import br.com.easypoint.dao.AdministradorDAO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import br.com.easypoint.model.Administrador;
import javax.servlet.http.HttpSession;

@ManagedBean(name = "MNGAdministrador")
@SessionScoped
public class MNGAdministrador {

    private Administrador administrador = new Administrador();

    public Administrador getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }

    public String verificaDados() throws Exception {
        AdministradorDAO administradorDAO = new AdministradorDAO();
        String resultado;

        try {

            this.administrador = administradorDAO.verificaDados(this.administrador);
            if (this.administrador != null) {

                FacesContext.getCurrentInstance().getExternalContext()
                        .getSessionMap().put("administrador", this.administrador);

                resultado = "/painel.xhtml?faces-redirect=true";

            } else {
                resultado = "/erro.xhtml?faces-redirect=true";
            }
        } catch (Exception e) {
            throw e;
        }

        return resultado;
    }

    public boolean verificaSessao() {
        boolean status;

        if (FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get("administrador") == null) {
            status = false;
        } else {
            System.out.println("ADM: " + FacesContext.getCurrentInstance().getExternalContext()
                    .getSessionMap().get("administrador").toString());
            status = true;
        }

        return status;
    }

    public String finalizaSessao() {
        FacesContext.getCurrentInstance().getExternalContext()
                .invalidateSession();
        return "/index.xhtml?faces-redirect=true";
    }

    public String menu(String url) {
        switch (url) {
            case "sair":
                return this.finalizaSessao();
            default:
                return null;
        }
    }
}
