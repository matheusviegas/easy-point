/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.HorarioDAO;
import br.com.easypoint.model.Horario;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Matheus Souza
 */
@ManagedBean(name = "MNGHorario")
@SessionScoped
public class MNGHorario implements Serializable {

    private Horario horario = new Horario();

    public Horario getHorario() {
        return horario;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }

    public void salvar() {
        HorarioDAO horarioDAO = new HorarioDAO();
        horarioDAO.salvar(this.horario);
        this.horario = new Horario();
    }

    public List<Horario> listar() {
        HorarioDAO horarioDAO = new HorarioDAO();
        return horarioDAO.listar();
    }

    public void remover(Horario horario) {
        HorarioDAO horarioDAO = new HorarioDAO();
        horarioDAO.remover(horario);
    }
}
