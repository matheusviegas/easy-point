/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.FuncionarioDAO;
import br.com.easypoint.dao.EnderecoDAO;
import br.com.easypoint.model.Endereco;
import br.com.easypoint.model.Estado;
import br.com.easypoint.model.Funcionario;
import br.com.easypoint.model.Pais;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Matheus Souza
 */
@ManagedBean(name = "MNGFuncionario")
@SessionScoped
public class MNGFuncionario implements Serializable {

    private final String base_url = "http://localhost:8080";
    private Funcionario funcionario = new Funcionario();
    private Endereco endereco = new Endereco();

    private Pais pais = new Pais();
    private Estado estado = new Estado();

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public void salvar() {
        FuncionarioDAO funcDAO = new FuncionarioDAO();
        EnderecoDAO endDAO = new EnderecoDAO();
        this.funcionario.setEndereco(endDAO.salvar(endereco));

        funcDAO.salvar(this.funcionario);
        this.funcionario = new Funcionario();
        this.endereco = new Endereco();
    }

    public List<Funcionario> listar() {
        FuncionarioDAO funcDAO = new FuncionarioDAO();
        return funcDAO.listar();
    }

    public void remover(Funcionario funcionario) {
        Endereco temp = new Endereco();
        temp = funcionario.getEndereco();
        FuncionarioDAO funcDAO = new FuncionarioDAO();
        funcDAO.remover(funcionario);
        EnderecoDAO endDAO = new EnderecoDAO();
        endDAO.remover(temp);
    }

    public String editar(Funcionario func) {
        this.endereco = func.getEndereco();
        this.pais = this.endereco.getCidade().getEstado().getPais();
        this.estado = this.endereco.getCidade().getEstado();

        return "editar.jsf";
    }

    public String navegar(String url) {

        String opcao = this.base_url + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();

        switch (url) {
            case "cadfuncionario":
                opcao += "/funcionarios/cadastrar.jsf";
                this.funcionario = new Funcionario();
                this.endereco = new Endereco();
                break;

            case "listfuncionario":
                opcao += "/funcionarios/listar.jsf";
                break;

            case "cadescala":
                opcao += "/escalas/cadastrar.jsf";
                break;

            case "cadhorario":
                opcao += "/horarios/cadastrar.jsf";
                break;
        }

        return opcao;
    }

}
