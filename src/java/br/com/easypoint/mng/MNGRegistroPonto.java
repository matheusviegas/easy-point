/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.easypoint.mng;

import br.com.easypoint.dao.RegistroPontoDAO;
import br.com.easypoint.model.RegistroPonto;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Matheus Souza
 */
@ManagedBean(name = "MNGRegistroPonto")
@SessionScoped
public class MNGRegistroPonto implements Serializable {

    private RegistroPonto registroponto = new RegistroPonto();

    public RegistroPonto getRegistroponto() {
        return registroponto;
    }

    public void setRegistroponto(RegistroPonto registroponto) {
        this.registroponto = registroponto;
    }

    public void salvar() {
        RegistroPontoDAO registropontoDAO = new RegistroPontoDAO();
        registropontoDAO.salvar(this.registroponto);
        this.registroponto = new RegistroPonto();
    }

    public List<RegistroPonto> listar() {
        RegistroPontoDAO registropontoDAO = new RegistroPontoDAO();
        return registropontoDAO.listar();
    }

    public void remover(RegistroPonto registroponto) {
        RegistroPontoDAO registropontoDAO = new RegistroPontoDAO();
        registropontoDAO.remover(registroponto);
    }
}
